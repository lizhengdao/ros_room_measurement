#include <ros/ros.h>
#include <iostream>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <ctime>
#include <thread>
#include <pcl/filters/extract_indices.h>
//#include <pcl/sample_consensus/ransac.h>
//#include <pcl/sample_consensus/sac_model_plane.h>
//#include <pcl/sample_consensus/sac_model_line.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/convert.h>
#include <geometry_msgs/TransformStamped.h>
#include <pcl/common/transforms.h>
#include <tf2_ros/static_transform_broadcaster.h>
//#include <pcl/filters/voxel_grid.h>
#include <Eigen/Dense>
#include <vector>

using namespace Eigen;
using namespace pcl;
using namespace std;

double ymin;
double ymax;
double xmin;
double xmax;
string filename;
int wallcount;

std::ofstream myfile;

ros::Publisher searchArea_pub;
ros::Publisher edgecloud_pub;
ros::Publisher corner_pub;

//store the edgecloud
pcl::PointCloud<pcl::PointXYZRGBA>::Ptr edgecloud (new pcl::PointCloud<pcl::PointXYZRGBA>);


//callback entered on input of a new PointCloud
void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& cloud_msg){
  //start timer for time measurement

  clock_t t_start = clock();
  ROS_INFO("Pointcloud Received");

  double x[wallcount];
  double y[wallcount];
  double z[wallcount];
  double xd[wallcount];
  double yd[wallcount];
  double zd[wallcount];



	//ROS_DEBUG_STREAM("Frame: " << cloud_msg->header.frame_id);
	//ROS_INFO("Frame: %s", cloud_msg->header.frame_id);
  //convert ROS Pointcloud to PCL Pointcloud
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::fromROSMsg(*cloud_msg, *cloud);

  //colorize search area for edgepoints
	pcl::PointCloud<pcl::PointXYZ>::Ptr searchArea (new pcl::PointCloud<pcl::PointXYZ>);

	pcl::PointCloud<pcl::PointXYZ> original_soil;

  for (int j = 0; j < cloud->size(); j++){
      pcl::PointXYZRGB point = cloud->at(j);
			if (point.x>xmin&&point.x<xmax){
				if(point.y>ymin&&point.y<ymax){
					searchArea->push_back(pcl::PointXYZ (point.x, point.y, point.z));
				}
			}
  }


  ROS_INFO("Pointcloud cut to relevant area");
	sensor_msgs::PointCloud2 searchArea_out;
	pcl::toROSMsg(*searchArea, searchArea_out);

  searchArea_out.header.frame_id = "/cloud";
	searchArea_out.header.stamp = cloud_msg->header.stamp;
	searchArea_pub.publish(searchArea_out);

  // define segmentation clouds

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr pclEdgeLine (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr pclFilterEdgeLine (new pcl::PointCloud<pcl::PointXYZ>);

  *cloud_filtered=*searchArea;

  for (int a=0;a<wallcount;a++){

    std::cerr << "running no" << a<< '\n';
    //initialise Line coefficients
    pcl::ModelCoefficients::Ptr coefficientsEdgeLine (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliersEdgeLine (new pcl::PointIndices ());

    pcl::SACSegmentation<pcl::PointXYZ> edge_seg;
    //To get good coefficients set this to true
    edge_seg.setOptimizeCoefficients (true);
    //Set Parameters for RANSAC
    edge_seg.setModelType (pcl::SACMODEL_LINE);
    edge_seg.setMethodType (pcl::SAC_RANSAC);
    edge_seg.setMaxIterations (2000);
    edge_seg.setDistanceThreshold (0.01);

    //set the Object that shall be filtered
    pcl::ExtractIndices<pcl::PointXYZ> extract;

    int i = 0, nrPointsEdgeLine = (int) cloud_filtered->points.size ();
    // While 80% of the original cloud is still there
    //lower values work worse.
    while (cloud_filtered->points.size () > 0.9 * nrPointsEdgeLine){
      // Segment the largest planar component from the remaining cloud
      edge_seg.setInputCloud (cloud_filtered);
      edge_seg.segment (*inliersEdgeLine, *coefficientsEdgeLine);

      if (inliersEdgeLine->indices.size () == 0){
        std::cerr << "Could not estimate a line model for the given dataset." << std::endl;
        return;
      }
      //Extract the Inliers
      extract.setInputCloud (cloud_filtered);
      extract.setIndices (inliersEdgeLine);
      extract.setNegative (false);
      extract.filter (*pclEdgeLine);
      //std::cerr << "PointCloud representing the line: " << pclEdgeLine->width * pclEdgeLine->height << " data points." << std::endl;

      //Update the filtering Object
      extract.setNegative (true);
      extract.filter (*pclFilterEdgeLine);
      cloud_filtered.swap (pclFilterEdgeLine);
      i++;
    }


    //push back a green marker for an accepted point of the EdgeCloud
    for (int i = 0; i< pclEdgeLine->size(); i++){
      pcl::PointXYZ acceptPoint = pclEdgeLine->at(i);

      pcl::PointXYZRGBA acceptedEdge;
      acceptedEdge.x=acceptPoint.x;
      acceptedEdge.y=acceptPoint.y;
      acceptedEdge.z=acceptPoint.z;
      acceptedEdge.r=255/(a+1);
      acceptedEdge.g=255-(255/(coefficientsEdgeLine->values[3]+2));
      acceptedEdge.b=255/(coefficientsEdgeLine->values[4]+2);
      acceptedEdge.a=(int)255;
      edgecloud->push_back (acceptedEdge);
    }

    for (int b=0;b<6;b++){
      //std::cerr << coefficientsEdgeLine->values[b]<< '\n';

    }
    x[a]=coefficientsEdgeLine->values[0];
    y[a]=coefficientsEdgeLine->values[1];
    xd[a]=coefficientsEdgeLine->values[3];
    yd[a]=coefficientsEdgeLine->values[4];


  }


  // a + m * c = e + n * g
  // b + m * d = f + n * h


  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cornercloud (new pcl::PointCloud<pcl::PointXYZRGBA>);



  for (int run1=0; run1 < wallcount; run1++){
    for (int run2=run1+1; run2 < wallcount; run2++){
      double n = (x[run1]-x[run2]+xd[run1]/yd[run1]*(y[run2]-y[run1])) /(xd[run2]-yd[run2]*xd[run1]/yd[run1]);

      //  (y[run2]-y[run1])*xd[run1]/yd[run1] - x[run2])/(xd[run2]-yd[run2]*xd[run1]/x[run1]);

      double xexp=x[run2]+n*xd[run2];
      double yexp=y[run2]+n*yd[run2];
      //std::cerr << xexp << "   "<<yexp << '\n';

      double firstangle=atan((x[run1]-xexp)/(y[run1]-yexp));
      double secondangle=atan((x[run2]-xexp)/(y[run2]-yexp));

      std::cerr << "angle " << (firstangle-secondangle)*100<< '\n';

      pcl::PointXYZRGBA cornerPoint;
      cornerPoint.x=xexp;
      cornerPoint.y=yexp;
      cornerPoint.z=std::abs(firstangle-secondangle)*3.60/(2*M_PI);
      cornerPoint.r=255;
      cornerPoint.g=0;
      cornerPoint.b=0;
      cornerPoint.a=255;

      myfile << cloud_msg->header.stamp <<"," <<xexp <<"," << yexp <<","<< std::abs(firstangle-secondangle)*360/(2*M_PI) << "\n";

      cornercloud->push_back(cornerPoint);
      sensor_msgs::PointCloud2 cornercloud_out;

      std::cerr << cornerPoint.x << '\n';
      pcl::toROSMsg(*cornercloud, cornercloud_out);

      cornercloud_out.header.frame_id = "/cloud";
    	cornercloud_out.header.stamp = cloud_msg->header.stamp;

      corner_pub.publish(cornercloud_out);
    }
  }





  ROS_INFO("Pointcloud cut to relevant area");
	sensor_msgs::PointCloud2 edgecloud_out;
	pcl::toROSMsg(*edgecloud, edgecloud_out);

  edgecloud_out.header.frame_id = "/cloud";
	edgecloud_out.header.stamp = cloud_msg->header.stamp;
	edgecloud_pub.publish(edgecloud_out);

  clock_t t_end = clock();
  double diff = 1000*(t_end - t_start) / CLOCKS_PER_SEC;
  ROS_INFO("calibrate: elapsed time in ms: %f", diff);
}

int main (int argc, char** argv){

  // Initialize ROS
  ros::init (argc, argv, "calibrate_node");
  ros::NodeHandle nh;

	nh.getParam("extract/ymin", ymin);
	nh.getParam("extract/ymax", ymax);
	nh.getParam("extract/xmin", xmin);
	nh.getParam("extract/xmax", xmax);
	nh.getParam("extract/filename", filename);
  nh.getParam("extract/wallcount", wallcount);


	myfile.open (filename);

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("/cloud", 10, cloud_cb);

	searchArea_pub= nh.advertise<sensor_msgs::PointCloud2> ("room_meas/searchArea", 10);

  edgecloud_pub= nh.advertise<sensor_msgs::PointCloud2> ("room_meas/edgecloud", 10);

  corner_pub= nh.advertise<sensor_msgs::PointCloud2> ("room_meas/corners", 10);

  // Spin to keep callbacks alive
  ros::spin ();

}
